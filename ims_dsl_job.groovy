job('ims_dsl_job') {
    scm {
        git {
            remote {
                url('https://antoniobarile@bitbucket.org/exeterdevelopers/imsintegration.git')
                credentials('antoniobarile@bitbucket.org')
            }
            createTag(false)
        }
    }
    triggers {
        scm('H/1 * * * *')
    }
    steps {
        maven('clean install')
        maven {
            goals('clean deploy -Dcarbon.url=https://ec2-34-251-3-39.eu-west-1.compute.amazonaws.com/api/v1/proxy/namespaces/default/services/esb-manager:servlet-http:9443 -Dcarbon.user=admin -Dcarbon.pass=admin')
            rootPOM("ImsCAR/pom.xml")
        }
    }
}