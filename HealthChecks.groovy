job('HealthChecks') {
    scm {
        git {
            remote {
		        url('https://bitbucket.org/exeterdevelopers/healthchecks.git')        
		    }
        }
    }
    triggers {
        scm('H/5 * * * *')
    }
    steps {
        maven('clean install')
        maven {
            goals('clean deploy -Dcarbon.url=https://esb-manager:9443 -Dcarbon.user=admin -Dcarbon.pass=admin')
            rootPOM("HealthChecksCAR/pom.xml")
        }
    }
}